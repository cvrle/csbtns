<!DOCTYPE HTML>

<html>

<head>
	<meta http-equiv="Content-Type"
	      content="text/html; charset=UTF-8"/>
	<title>Custom share buttons</title>
	<meta property="og:image" content="http://img.yasmina.com/HQVi3FV6QPQoVKNkDuGrkEGYttQ=/640x480/smart/http://www.kaltura.com/p/676152/thumbnail/entry_id/1_3pp2rbpx/width/640/height/480/a.jpg">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

	<script type="text/javascript"
	        src="Sharrre/jquery.sharrre.js"></script>

	<link href="style.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,300,800' rel='stylesheet' type='text/css'>

</head>

<body>
<div class="container">
	<h1>Custom share buttons</h1>
	<?php
		$url = urlencode('http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']);
		$url = str_replace(array('%2F', '%3A', '%25', '%3F'), array('/', ':', '%', '?'), $url);
	?>
	<div class="social_share">
		<div id="facebook_share" class="share_btn"
		     data-url="<?php echo $url ?>"
		     data-text="SHARE TEXT"></div>

		<div id="twitter_share" class="share_btn"
		     data-url="<?php echo $url ?>"
		     data-text="SHARE TEXT"></div>

		<div id="gplus_share" class="share_btn"
		     data-url="<?php echo $url ?>"
		     data-text="SHARE TEXT"></div>

		<!-- Place this tag in your head or just before your close body tag. -->
		<script src="https://apis.google.com/js/platform.js" async defer></script>

		<!-- Place this tag where you want the share button to render. -->
		<div class="g-plus" data-action="share" data-annotation="vertical-bubble" data-height="60" data-href="http://cvrle.info/csbtns/"></div>
	</div>


<!--	<div id="fb-root"></div>-->
<!--	<script>(function(d, s, id) {-->
<!--			var js, fjs = d.getElementsByTagName(s)[0];-->
<!--			if (d.getElementById(id)) return;-->
<!--			js = d.createElement(s); js.id = id;-->
<!--			js.src = "//connect.facebook.net/sr_RS/sdk.js#xfbml=1&version=v2.3";-->
<!--			fjs.parentNode.insertBefore(js, fjs);-->
<!--		}(document, 'script', 'facebook-jssdk'));</script>-->
<!---->
<!--	<div class="fb-share-button" data-href="http://--><?php //echo $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'] ?><!--" data-layout="button_count"></div>-->
</div>
</body>

<script>
	$('#facebook_share').sharrre({
		share: {
			facebook: true
		},
		template: '<a class="box" href="#"><div class="count">{total}</div><div ' +
		'class="share"><span>fb</span></div></a>',
		enableHover: false,
		enableTracking: false,
		buttons: {
			facebook: {
				lang: 'ar_AR'
			}
		},
		click: function (api, options) {
			api.openPopup('facebook');
		}
	});

	$('#twitter_share').sharrre({
		share: {
			twitter: true
		},
		template: '<a class="box" href="#"><div class="count">{total}</div><div ' +
		'class="share"><span>t</span></div></a>',
		enableHover: false,
		enableTracking: false,
		click: function (api, options) {
			api.openPopup('twitter');
		}
	});

	$('#gplus_share').sharrre({
		share: {
			googlePlus: true
		},
		template: '<a class="box" href="#"><div class="count">{total}</div><div ' +
		'class="share"><span>g+</span></div></a>',
		enableHover: false,
		enableTracking: false,
		buttons: {
			googlePlus: {
				lang: 'ar'
			}
		},
		urlCurl: 'Sharrre/sharrre.php',
		click: function (api, options) {
			api.openPopup('googlePlus');
		}
	});
</script>



</html>